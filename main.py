import json
from pymongo import MongoClient

# import libraries
from datetime import date
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def collection(uri):
    client = MongoClient(uri)
    database = client["rhobs"]
    collection = database["rhobs"]
    return collection


def load(uri="localhost", datapath="./data.json"):
    coll = collection(uri=uri)
    with open(datapath, "r") as fp:
        data = json.load(fp)
        for person in data:
            coll.insert_one(person)
            print(person)


# 3-a
def Q_3_a():
    countAll = mycol.count_documents({})
    countMales = mycol.count_documents({"sex": "M"})
    countFemales = mycol.count_documents({"sex": "F"})
    print(
        f" count all  = {countAll} \n countMales = {countMales} \n countFemales = {countFemales} \n ***TEST*** Verification que c'est la somme exact : {countMales }+{ countFemales }=  {countMales + countFemales }"
    )


# 3-b
def Q_3_b(N=0):  # 0 comme valeur par defaut
    dictioCompanies = dict()  # contains all elements [{"comanyName":occurences}]
    for person in mycol.find():
        company = person["company"]
        if company in dictioCompanies:
            #         increment current value
            dictioCompanies[company] += 1
        else:
            dictioCompanies[company] = 1
    dictBiggerThanN = dict()  # will contains only element filtered
    countCompany = 0  # for test
    for (key, value) in dictioCompanies.items():
        countCompany += value  # we verify that sum(values) ==  countCompanies == 50k
        if value > N:
            dictBiggerThanN[key] = value
    print(
        f" List of companies that has more than {N} person (with the exact occurrence) : \n {dictBiggerThanN}\n\n"
    )
    print(
        f" List of companies that has more than {N} person (with the exact occurrence) : \n {dict(sorted(dictBiggerThanN.items(), key=lambda item: item[1]))}"  # lambda arguments : expression
    )

    print(f" ***TEST*** total somme of values = {countCompany}")


#  3-c
def Q_3_c(
    jobTitle="géologue minier",
):  # j'ai choisi ce metier comme un metier par defaut
    agesFemale = []
    agesMale = []
    today = date.today()  # pour calculer l'age
    # find and filter persons with exact job 'by gender'
    for person in mycol.find():
        job = person["job"]
        if jobTitle == job:
            # calculate age from birthday
            age = (
                today.year
                - int(person["birthdate"][0:4])
                - (
                    (today.month, today.day)
                    < (int(person["birthdate"][5:7]), int(person["birthdate"][8:]))
                )
            )
            if person["sex"] == "M":
                agesMale.append(age)
            else:
                agesFemale.append(age)
    agesMale.sort()
    agesFemale.sort()
    print(f"ages of Male that has job == {jobTitle} : \n {agesMale}")
    print(f"ages of Female that has job == {jobTitle} : \n {agesFemale}")
    matPlotLib(agesMale, agesFemale, jobTitle)


def matPlotLib(
    AgesMale, AgesFemale, jobTitle
):  # || and here is why the SORTED IS SOO IMPORTANT]
    # notre intervalle des ages que j'ai choisi, on peut le changer sans probleme, just on doit respecter la form "XX-XX"
    Age = ["0-7", "8-17", "18-25", "26-40", "41-60", "61-200"]
    # les deux list ou on va sauvegarder les occurrences des ages
    OccurrenceAgeMale = []
    OccurrenceAgeFemale = []
    # deux variables qu'on va utiliser pour calculer les occurences dans les deux listes des ages qu'on a en entree[arguments]
    # occurrence dans la liste des ages des HOMMES
    AgeTrigger = 0
    countAges = 0
    for ageMale in AgesMale:
        if ageMale >= int(Age[AgeTrigger].split("-")[0]) and ageMale <= int(
            Age[AgeTrigger].split("-")[
                1
            ]  # if current ageMale is in the first Age element interval
        ):
            countAges += 1
        else:
            AgeTrigger += 1  # pass to next element in Age ,
            # and here is why the SORTED IS SOO IMPORTANT
            OccurrenceAgeMale.append(countAges)
            countAges = 0
            # and we can't just lose this element ,
            # so we start incrementing again ..
            if ageMale >= int(Age[AgeTrigger].split("-")[0]) and ageMale <= int(
                Age[AgeTrigger].split("-")[1]
            ):
                countAges += 1
    OccurrenceAgeMale.append(
        countAges
    )  # adding the last elements count because he wont be executed in else , only if and go out from loop

    # occurrence dans la liste des ages des FEMMES
    AgeTrigger = 0
    countAges = 0
    for ageFemale in AgesFemale:
        if ageFemale >= int(Age[AgeTrigger].split("-")[0]) and ageFemale <= int(
            Age[AgeTrigger].split("-")[1]
        ):
            countAges += 1
        else:
            AgeTrigger += 1
            OccurrenceAgeFemale.append(countAges)
            countAges = 0
            if ageFemale >= int(Age[AgeTrigger].split("-")[0]) and ageFemale <= int(
                Age[AgeTrigger].split("-")[1]
            ):
                countAges += 1
    OccurrenceAgeFemale.append(countAges)  # adding the last elements count

    print(
        f'''"occurrence des ages pour les HOMMES de  : {Age} \n {OccurrenceAgeMale}"'''
    )
    print(
        f'''"occurrence des ages pour les FEMMES de  : {Age} \n {OccurrenceAgeFemale}"'''
    )

    # create dataframe[like a table(excel for exemple)]
    df = pd.DataFrame(
        {"Age": Age, "Male": OccurrenceAgeMale, "Female": OccurrenceAgeFemale}
    )
    # preparing the graph
    print(df.head)
    print(len(df))
    # define x and y limits
    y = range(0, len(df))
    x_male = df["Male"]
    x_female = df["Female"]
    # define plot parameters
    fig, axes = plt.subplots(ncols=2, sharey=True, figsize=(9, 6))
    # specify background color and plot title
    fig.patch.set_facecolor("xkcd:light grey")
    plt.figtext(0.5, 0.9, f"Population Pyramid : {jobTitle}", fontsize=11, ha="center")

    # define male and female bars
    axes[0].barh(y, x_male, align="center", color="royalblue")
    axes[0].set(title="Males")
    axes[1].barh(y, x_female, align="center", color="lightpink")
    axes[1].set(title="Females")

    # adjust grid parameters and specify labels for y-axis
    axes[1].grid()
    axes[0].set(yticks=y, yticklabels=df["Age"])
    axes[0].invert_xaxis()
    axes[0].grid()

    # display plot
    plt.show()


# Ton challenge si tu l'acceptes, est :
# 1. D'installer une base de données Mongodb => Bien Fait ✔
# 2. D'insérer les données du fichier joint (tu peux t'aider du code ci-dessus).
#   Bien Fait ✔
# 3. Répondre aux exercices suivants en écrivant le code python :
#        a. Compter le nombre de femmes / d'hommes.
#        b. Écrire une fonction qui renvoie les entreprises de plus de N personnes.
#        c. Écrire une fonction qui prend en paramètre un métier et qui renvoie la pyramide des âges pour ce métier.

# calls Section
# load()  #bien fait

# Connect to DataBase
####
myclient = MongoClient("mongodb://localhost:27017/")
mydb = myclient["rhobs"]
mycol = mydb["rhobs"]
####
# print("Qestion 3_a : \n")
# Q_3_a()
# print("\nQestion 3_b : \n")
# # integer input
# N = int(input("Enter N : (les entreprises de plus de N personnes)\n"))
# Q_3_b(N)
print("\nQestion 3_c : \n")
# job = input('Enter the Job: (eg : "géologue minier")\n')
job = "géologue minier"
Q_3_c(job)
