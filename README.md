# Ton challenge si tu l'acceptes, est :
1. D'installer une base de données Mongodb => Bien Fait ✔
2. D'insérer les données du fichier joint (tu peux t'aider du code ci-dessus).
  Bien Fait ✔
3. Répondre aux exercices suivants en écrivant le code python :


       a. Compter le nombre de femmes / d'hommes.
       b. Écrire une fonction qui renvoie les entreprises de plus de N personnes.
       c. Écrire une fonction qui prend en paramètre un métier et qui renvoie la pyramide des âges pour ce métier.

# Execution results:

```

PS C:\Users\Reda El Ouahabi\Desktop\New folder> python .\main.py
Qestion 3_a :

 count all  = 50000
 countMales = 24958
 countFemales = 25042
 ***TEST*** Verification que c'est la somme exact : 24958+25042=  50000

Qestion 3_b :

 List of companies that has more than 65 person (with the exact occurrence) :
 {'Gautier': 67, 'Hamon': 73, 'Colas': 76, 'Cousin': 68, 'Guilbert': 76, 'Bouvet': 67, 'Maillot': 69, 'Poirier': 67, 'Ferreira': 66, 'Prévost': 67, 'Chevallier': 67, 'Munoz': 66, 'Riou': 69, 'Laurent': 75, 'Brunet': 69, 'Bonnet': 82, 'De Oliveira': 67, 'Perez': 73, 'Bigot': 70, 'Valentin': 74, 'Guyon': 66, 'Alexandre': 69, 'Bailly': 75, 'Coste': 77, 'Lebreton': 69, 'Rodriguez': 77, 'Delannoy': 68, 'Lelièvre': 66, 'Thierry': 66, 'Legendre': 72, 'Martin': 66, 'Bourgeois': 72, 'Guillot': 68, 'Gaillard': 72, 'Mathieu': 71, 'Pages': 77, 'Rousset': 80, 'Lenoir': 71, 'Roussel': 79, 'Leclercq': 81, 'Lagarde': 66, 'Camus': 76, 'Boulanger': 66, 'Gros': 75, 'Marty': 70, 'Faure': 75, 'Gilbert': 74, 'Charrier': 74, 'Voisin': 70, 'Grenier': 71, 'Georges': 67, 'Fleury': 69, 'Cohen': 71, 'Leblanc': 68, 'Poulain': 67, 'Benard': 68, 'Ollivier': 74, 'Hoareau': 68, 'Laine': 74, 'Chauveau': 66, 'Duval': 66, 'Rey': 69, 'Rocher': 66, 'Diaz': 67, 'Garnier': 73, 'Marques': 73, 'Evrard': 83, 'Durand': 79, 'Vallet': 71, 'Bernard': 81, 'Vallée': 72, 'Pottier': 81, 'Briand': 70, 'Bourdon': 72, 'Imbert': 72, 'Lombard': 73, 'Le Gall': 66, 'Torres': 66, 'Techer': 68, 'Renaud': 73, 'Julien': 67, 'Wagner': 73, 'Letellier': 66, 'Gay': 84, 'Da Silva': 66, 'Noël': 71, 'Martinez': 78, 'Legros': 68, 'Gosselin': 71, 'Henry': 67, 'Bouchet': 66, 'Rodrigues': 69, 'Barthelemy': 67, 'Hubert': 74, 'Maréchal': 68, 'Colin': 80, 'Clerc': 73, 'Dijoux': 76, 'Normand': 82, 'Monnier': 81, 'Guichard': 76, 'Buisson': 67, 'Lambert': 68, 'Langlois': 71, 'Baudry': 68, 'Rolland': 72, 'Morin': 67, 'Dufour': 70, 'Roy': 70, 'Le Goff': 68, 'Launay': 75, 'Nicolas': 68, 'Lévy': 69, 'Morel': 69, 'Blanc': 75, 'Guibert': 67, 'Pasquier': 69, 'Perrot': 67, 'Leduc': 74, 'Caron': 69, 'Lacroix': 66, 'Moulin': 71, 'Albert': 76, 'Olivier': 69, 'Hervé': 74, 'Blin': 67, 'Martins': 78, 'Roux': 66, 'Tessier': 69, 'Gauthier': 68, 'Guillou': 67, 'Duhamel': 75, 'Jacques': 73, 'Lecomte': 78, 'Sanchez': 66, 'Brun': 69, 'Raymond': 74, 'Guillaume': 66}
 ***TEST*** total somme of values = 50000

Qestion 3_c :

ages of Male that has job == géologue minier :
 [1, 3, 7, 12, 14, 20, 23, 24, 26, 27, 29, 40, 42, 43, 44, 45, 45, 47, 48, 60, 62, 64, 65, 69, 72, 73, 75, 76, 93, 96, 99, 108, 110, 113, 115]
ages of Female that has job == géologue minier :
 [5, 6, 8, 11, 12, 13, 24, 25, 27, 31, 36, 41, 41, 49, 53, 53, 64, 70, 74, 75, 76, 77, 79, 80, 82, 87, 89, 90, 95, 95, 101, 101, 102, 103, 109]
"occurrence des ages pour les HOMMES de  : ['0-7', '8-17', '18-25', '26-40', '41-60', '61-200']
 [3, 2, 3, 4, 8, 15]"
"occurrence des ages pour les FEMMES de  : ['0-7', '8-17', '18-25', '26-40', '41-60', '61-200']
 [2, 4, 2, 3, 5, 19]"

```
# Pyramide des âges (metier =="géologue minier")

![graph](./Figure_1.png)

